package cl.tinet.qa.wom.appium.ios;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.microsoft.appcenter.appium.EnhancedIOSDriver;
import com.microsoft.appcenter.appium.Factory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class TestExampleiOS {
	
	private static EnhancedIOSDriver<MobileElement> driver;
	
	@Rule
    public TestWatcher watcher = Factory.createWatcher();
	
	@BeforeClass
	public static void setup() throws MalformedURLException {
		DesiredCapabilities	caps = new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.0");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6s");
		caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
		
		caps.setCapability(MobileCapabilityType.APP, "/Users/miguel/Work/Tinet/qa/wom/appium-wom/AppWomMobile.ipa");
		URL url = new URL("http://localhost:4723/wd/hub");
		
		driver = Factory.createIOSDriver(url, caps);
	}
	
	@Test
	public void test_validarBolsas() {
		driver.label("Presionar Boton Empezar");
		MobileElement el1 = (MobileElement) driver.findElementByAccessibilityId("btnEmpezar");
		el1.click();
		
		driver.label("Ingresando Numero de Telefono: 949407211");
		MobileElement el2 = (MobileElement) driver.findElementByAccessibilityId("txtFieldNumeroTelefono");
		el2.click();
		el2.sendKeys("949407211");
		
		driver.label("Boton Solicitar Codigo");
		MobileElement el3 = (MobileElement) driver.findElementByAccessibilityId("btn_Solicitar_Codigo");
		el3.click();
		
		driver.label("Ingresando Codigo Verificacion: 9948");
		MobileElement el4 = (MobileElement) driver.findElementByAccessibilityId("inputCodigoVerificacion");
		el4.click();
		el4.sendKeys("9448");
		
		driver.label("Boton Verificar");
		MobileElement el5 = (MobileElement) driver.findElementByAccessibilityId("btnVerificar");
		el5.click();
		
		driver.label("Ingresando Nombre: JP");
		MobileElement el6 = (MobileElement) driver.findElementByAccessibilityId("inputNombre");
		el6.click();
		el6.sendKeys("JP");
		
		driver.label("Ingresando Apellido: Madariaga");
		MobileElement el7 = (MobileElement) driver.findElementByAccessibilityId("inputApellido");
		el7.click();
		el7.sendKeys("Madariaga");
		
		driver.label("Boton Comenzar");
		MobileElement el8 = (MobileElement) driver.findElementByAccessibilityId("btnComenzar");
		el8.click();
		
		driver.label("Click VOZ");
		MobileElement el10 = (MobileElement) driver.findElementByAccessibilityId("VOZ");
		el10.click();
		
		driver.label("Click SMS");
		MobileElement el11 = (MobileElement) driver.findElementByAccessibilityId("SMS");
		el11.click();
		
		driver.label("Click DATOS");
		MobileElement el9 = (MobileElement) driver.findElementByAccessibilityId("DATOS");
		el9.click();
		
		driver.label("Click Bolsas");
		MobileElement el12 = (MobileElement) driver.findElementByAccessibilityId("Bolsas");
		el12.click();
		
		driver.label("Check Boton Bolsas Not Null");
		Assert.assertNotNull(el12);
	}
	
	@AfterClass
	public static void tearDown() {
		driver.label("Cerrando Aplicacion ...");
		driver.quit();
	}
}

